<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcd58ac4ad41e8657522639ee8ebc5f1a
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'Pondit\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Pondit\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitcd58ac4ad41e8657522639ee8ebc5f1a::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitcd58ac4ad41e8657522639ee8ebc5f1a::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
